import requests
import json

# Results from previous task
snowInstance = morpheus['results']['snowInstance']

# Var from previous task. 
instance_id = snowInstance['customOptions']['morpheusInstances']

# Morpheus Variables
api_url = morpheus['morpheus']['applianceUrl']
bearer_token = morpheus['morpheus']['apiAccessToken']

# Custom Options Variables
desired_plan_id = morpheus['customOptions']['planOptions']

headers = {
    'Authorization': f'Bearer {bearer_token}',
    'Content-Type': 'application/json'
}

def get_vm_details():
    url = f'{api_url}/api/instances/{instance_id}'
    response = requests.get(url, headers=headers, verify=False)
    return json.dumps(response.json(), indent=4)

def resize_vm():
    url = f'{api_url}/api/instances/{instance_id}/resize'
    payload = {
        "instance": {
            "plan": {
            "id": desired_plan_id
            }
        }
    }
    response = requests.put(url, headers=headers, data=json.dumps(payload), verify=False)
    return json.dumps(response.json(), indent=4)

if __name__ == '__main__':
    vm_details = get_vm_details()
    vm_resize = resize_vm()
    print(vm_resize)