import requests
import json
 
# Results from previous task
snowInstance = morpheus['results']['snowInstance']
 
# Var from previous task. 
instance_id = snowInstance['customOptions']['morpheusInstances']
 
# Morpheus Variables
api_url = morpheus['morpheus']['applianceUrl']
bearer_token = morpheus['morpheus']['apiAccessToken']
 
 
headers = {
    'Authorization': f'Bearer {bearer_token}',
    'Content-Type': 'application/json'
}
 
# get the instance details
def get_vm_details():
    url = f'{api_url}/api/instances/{instance_id}'
    response = requests.get(url, headers=headers, verify=False)
    return json.dumps(response.json(), indent=4)
 
# delete the instance
def delete_instance():
    url = f'{api_url}/api/instances/{instance_id}'
    response = requests.delete(url, headers=headers, verify=False)
    return json.dumps(response.json(), indent=4)

if __name__ == '__main__':
    vm_details = get_vm_details()
    vm_delete = delete_instance()
    print(vm_delete)